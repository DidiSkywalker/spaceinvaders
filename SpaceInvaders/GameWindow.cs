﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using static SpaceInvaders.Settings;

namespace SpaceInvaders {
    public partial class GameWindow : Form {

        public Input input;
        private Game game;

        public GameWindow() {
            InitializeComponent();
            
            // Enable DEBUG MODE
            DEBUG = true;

            gameTimer.Interval = 1000 / Settings.FPS;
            gameTimer.Tick += update;
            gameTimer.Start();

            // Neuer Input mit gewünschten Keys
            input = new Input(new int[] {
                (int)Keys.A,
                (int)Keys.D,
                (int)Keys.Space,
                (int)Keys.Escape
            });

            game = new Game(this);
        }

        // Update Methode
        public void update(object sender, EventArgs e) {
            // Update
            game.update();

            // Invalidate Canvas to cause repaint next tick.
            canvas.Invalidate();
        }

        // Draw Methode
        private void canvas_Paint(object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            // Clear
            g.Clear(Settings.BACKGROUND_COLOR);
            // Draw
            game.draw(g);
        }

// ----------------------------------------------------------------------------------------------------
        
        // GETTER
        public Input getInput() {
            return input;
        }

// ----------------------------------------------------------------------------------------------------

        // INPUT
        private void GameWindow_KeyDown(object sender, KeyEventArgs e) {
            input.onKeyDown(e);
        }

        private void GameWindow_KeyUp(object sender, KeyEventArgs e) {
            input.onKeyUp(e);
        }

        private void GameWindow_MouseDown(object sender, MouseEventArgs e) {
            input.onMouseDown(e);
        }

        private void GameWindow_MouseUp(object sender, MouseEventArgs e) {
            input.onMouseUp(e);
        }

        private void GameWindow_MouseMove(object sender, MouseEventArgs e) {
            input.onMouseMove(e);
        }

// ----------------------------------------------------------------------------------------------------

        // DEBUG HANDLING
        #region DEBUG HANDLING
        private void GameWindow_Load(object sender, EventArgs e) {
            if (Settings.DEBUG) AllocConsole();
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        #endregion
    }
}
