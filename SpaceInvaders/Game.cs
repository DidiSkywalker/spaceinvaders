﻿using System.Drawing;
using System.Windows.Forms;
using static SpaceInvaders.Settings;

namespace SpaceInvaders {
    class Game {

        // Eigene instanz
        private static Game instance;

        private GameWindow gameWindow;

        public Game(GameWindow gameWindow) {
            instance = this;
            this.gameWindow = gameWindow;
        }

        // Update Methode
        public void update() {

        }

        // Draw Methode
        public void draw(Graphics g) {

        }

        #region shortcut getters
        public Input getInput() {
            return gameWindow.getInput();
        }
        #endregion

        public static Game getInstance() {
            return instance;
        }
    }
}
