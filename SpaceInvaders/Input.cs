﻿using System.Windows.Forms;
using System.Drawing;

namespace SpaceInvaders {
    public class Input {

        private bool[] keys = new bool[255];
        private bool mouseDown = false;
        private Point mouseLocation = new Point();

        public Input(int[] initialKeys) {
            foreach (int i in initialKeys) {
                keys[i] = false;
            }
        }

        public void onKeyDown(KeyEventArgs e) {
            keys[e.KeyValue] = true;
        }

        public void onKeyUp(KeyEventArgs e) {
            keys[e.KeyValue] = false;
        }

        public void onMouseDown(MouseEventArgs e) {
            mouseDown = true;
        }

        public void onMouseUp(MouseEventArgs e) {
            mouseDown = false;
        }

        public void onMouseMove(MouseEventArgs e) {
            mouseLocation = e.Location;
        }

        public bool get(int keyCode) {
            return keys[keyCode];
        }

        public bool get(Keys k) {
            return get((int)k);
        }

        public bool isMouseDown() {
            return mouseDown;
        }

        public Point getMouseLocation() {
            return mouseLocation;
        }
    }
}
