﻿using System;

namespace SpaceInvaders {
    class Logger {

        public static void log(String msg) {
            Console.WriteLine(">> " + msg);
        }

        public static void d(String msg) {
            log("D: " + msg);
        }

        public static void w(String msg) {
            log("WARN: " + msg);
        }

        public static void e(String msg) {
            log("ERROR: " + msg);
        }

        public static void d(String tag, String msg) {
            log(tag + "/D: " + msg);
        }

        public static void w(String tag, String msg) {
            log(tag + "/WARN: " + msg);
        }

        public static void e(String tag, String msg) {
            log(tag + "/ERROR: " + msg);

        }
    }
}
