﻿using System.Drawing;

namespace SpaceInvaders {
    public static class Settings {

        // Debug Mode
        public static bool DEBUG { get; set; } = false;

        // Canvas Breite
        public static int WIDTH { get; } = 1080;

        // Canvas Höhe
        public static int HEIGHT { get; } = 720;

        // FPS
        public static int FPS { get; } = 60;

        // Hintergrund Farbe
        public static Color BACKGROUND_COLOR { get; } = Color.FromArgb(174, 203, 249);

        // Spieler Farbe
        public static Color PLAYER_COLOR { get; } = Color.Black;

        // Invader Farbe
        public static Color INVADER_COLOR { get; } = Color.Red;
    }
}
